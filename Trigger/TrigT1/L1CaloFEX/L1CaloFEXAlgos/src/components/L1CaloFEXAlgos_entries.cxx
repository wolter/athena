/*
    Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "../jFexTower2SCellDecorator.h"

using namespace LVL1;

DECLARE_COMPONENT( jFexTower2SCellDecorator )
